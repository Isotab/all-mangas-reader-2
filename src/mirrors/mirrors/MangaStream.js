// TEST!!!
if (typeof registerMangaObject === 'function') {
    registerMangaObject({
        mirrorName: "MangaStream",
        mirrorIcon: "mangastream.png",
        languages: "en",
        domains: ["www.mangastream.cc"],
        home: "https://www.mangastream.cc/",
        chapter_url: /^\/manga\/.*\/.*$/g,
        canListFullMangas: false,
        abstract: "Madara",
        abstract_options: {
            search_url: "https://www.mangastream.cc/"
        }
    })
}
/*
if (typeof registerMangaObject === 'function') {
	registerMangaObject({
        mirrorName: "MangaStream",
        canListFullMangas: true,
        mirrorIcon: "mangastream.png",
        languages: "en",
        domains: ["mangastream.com","readms.com","readms.net", "mangastream.cc"],
        home: "https://www.mangastream.cc/",
        chapter_url: /\/manga\//g,

        getMangaList: async function (search) {
            let doc = await amr.loadPage("https://www.mangastream.cc/manga", { nocache: true, preventimages: true })
            let res = [];
            $('.table-striped strong a', doc).each(function (index) {
                res[index] = [$(this).text().trim(), "https://www.mangastream.cc" + $(this).attr('href')];
            });
           return res;
        },
    
        getListChaps: async function (urlManga) {
            let doc = await amr.loadPage(urlManga, { nocache: true, preventimages: true })
            let res = [];
            $('.table-striped a', doc).each(function () {
                res[res.length] = [$(this).text().trim(), "https://www.mangastream.cc" + $(this).attr("href")];
            });
            return res;
        },
    
        getInformationsFromCurrentPage: async function (doc, curUrl) {
            var name = $('.dropdown-toggle .hidden-sm:first', doc).text(),
                currentMangaURL = "https://www.mangastream.cc" + $('.btn-group .dropdown-menu:first a:last', doc).attr('href'),
                currentChapterURL = "https://www.mangastream.cc" + $('.dropdown-menu:last a:first', doc).attr('href');
            return {
                "name": name,
                "currentMangaURL": currentMangaURL,
                "currentChapterURL": currentChapterURL
            }
        },
    
        getListImages: async function (doc, curUrl) {
            var res = [];
            var last = $('.dropdown-menu:last li a:last', doc);
            var npages = parseInt(last.text().replace(/[^0-9]/g, ''));
            var baseUrl = last.attr('href').replace(/\/[^/]*$/g, '/');
            while (npages > 0)
            {
                res[npages - 1] = "https://www.mangastream.cc" + baseUrl + npages;
                npages--;
            }
            return res;
        },
    
        getImageFromPageAndWrite: async function (urlImg, image) {
            let doc = await amr.loadPage(urlImg)
            src = "https:" + $("img#manga-page", doc).attr("src");
            $(image).attr("src", src);
        },
        
        isCurrentPageAChapterPage: function (doc, curUrl) {
            return ($("#manga-page", doc).length > 0);
        }
    })
}*/